/**
 * 
 */
package org.evolvis.taskmanager.swt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.evolvis.taskmanager.TaskManager;
import org.evolvis.taskmanager.TaskManager.Context;
import org.evolvis.taskmanager.TaskManager.TaskListener;

/**
 * Simple implementation of the {@link TaskManager.TaskListener} interface which
 * displays the latest task with a label and a progress bar and provides a
 * cancel button for cancelable tasks.
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SmallTaskManagerPanel extends Composite implements TaskListener {

	protected Label label;
	
	protected Button cancelButton;
	protected ProgressBar bar;

	protected String description;
	
	protected List<Context> contexts = Collections.synchronizedList(new ArrayList<Context>());
	
	protected Context showingContext;
	
	public SmallTaskManagerPanel(Composite parent, int style) {
		super(parent, style);
		setLayout(new MigLayout("", "[grow, fill] [grow, fill] [grow]"));
		label = new Label(this, SWT.NONE);
		label.setText("bla");
		bar = new ProgressBar (this, SWT.HORIZONTAL | SWT.INDETERMINATE);
		cancelButton = new Button(this, SWT.NONE);
		cancelButton.setText("X");
		
		// hide all components first
		label.setVisible(false);
		bar.setVisible(false);
		cancelButton.setVisible(false);
	}

	/**
	 * @see org.evolvis.taskmanager.TaskManager.TaskListener#activityDescriptionSet(org.evolvis.taskmanager.TaskManager.Context, java.lang.String)
	 */
	public void activityDescriptionSet(Context context, String description) {
		if (context == showingContext)
			setLabelLater(description);
	}

	/**
	 * @see org.evolvis.taskmanager.TaskManager.TaskListener#blockingTaskRegistered(org.evolvis.taskmanager.TaskManager.Context, java.lang.String)
	 */
	public void blockingTaskRegistered(Context context, String description) {
		taskRegistered(context, description);
	}

	/**
	 * @see org.evolvis.taskmanager.TaskManager.TaskListener#currentUpdated(org.evolvis.taskmanager.TaskManager.Context, int)
	 */
	public void currentUpdated(Context context, int current) {
		if (context == showingContext)
			setProgressLater(current);
	}

	/**
	 * @see org.evolvis.taskmanager.TaskManager.TaskListener#exclusiveTaskRegistered(org.evolvis.taskmanager.TaskManager.Context, java.lang.String)
	 */
	public void exclusiveTaskRegistered(Context context, String description) {
		taskRegistered(context, description);
	}

	/**
	 * @see org.evolvis.taskmanager.TaskManager.TaskListener#goalUpdated(org.evolvis.taskmanager.TaskManager.Context, int)
	 */
	public void goalUpdated(Context context, int goal) {
		if (context == showingContext)
			setMaximumLater(goal);
	}

	/**
	 * @see org.evolvis.taskmanager.TaskManager.TaskListener#taskCancelled(org.evolvis.taskmanager.TaskManager.Context)
	 */
	public void taskCancelled(Context context) {
		taskCompleted(context);
	}

	/**
	 * @see org.evolvis.taskmanager.TaskManager.TaskListener#taskCompleted(org.evolvis.taskmanager.TaskManager.Context)
	 */
	public void taskCompleted(Context context) {
		if (context == showingContext)	{
			
			getDisplay().asyncExec(new Runnable() {
				
				public void run() {
					label.setText("");
					label.setVisible(false);
					bar.setVisible(false);
					cancelButton.setVisible(false);
					//bar.setMaximum(0);
				}
			});
		}

		removeContext(context);
	}

	/**
	 * @see org.evolvis.taskmanager.TaskManager.TaskListener#taskRegistered(org.evolvis.taskmanager.TaskManager.Context, java.lang.String)
	 */
	public void taskRegistered(Context context, String description) {
		addContext(context);
		this.description = description;
	}

	/**
	 * @see org.evolvis.taskmanager.TaskManager.TaskListener#taskStarted(org.evolvis.taskmanager.TaskManager.Context)
	 */
	public void taskStarted(Context context) {
		setProgressLater(0);
	}
	
	/**
	 * Sets the progress bar on the Swing thread.
	 * 
	 * <p>If <code>progress</code> is <code>0</code> this
	 * is interpreted as the task's start which results in
	 * the label, the progress bar and the cancel button (if
	 * applicable) being made visible.</p>
	 * 
	 * <p>If the progress bar's maximum value is zero it is
	 * set into indeterminate mode.</p>
	 * 
	 * <p>In case <code>progress</code> is non-zero the progress
	 * bar's value is updated accordingly.</p>
	 * 
	 * @param progress
	 */
	private void setProgressLater(final int progress)
	{
		getDisplay().asyncExec(new Runnable()
		{
			public void run()
			{
				if (progress == 0) {
					label.setText(description);
					label.setVisible(true);
					bar.setVisible(true);
					cancelButton.setVisible(showingContext != null && showingContext.isCancelable());
					cancelButton.setEnabled(true);
				}
				else
					bar.setSelection(progress);
			}
		});
	}
	
	private void setMaximumLater(final int maximum)
	{
		getDisplay().asyncExec(new Runnable() {
			
			public void run()
			{
				//bar.setIndeterminate(false);
				//bar.setMaximum(maximum);
			}
		});
	}

	/** Sets the label's text (on the Swing thread).
	 * 
	 * @param text
	 */
	private void setLabelLater(final String text)
	{
		getDisplay().asyncExec(new Runnable() {
			
			public void run() {
				label.setText(text);
			}
		});
	}
	
	private void addContext(Context cntx) {
		contexts.add(cntx);
		showingContext = getMostImportantContext();
	}

	private void removeContext(Context cntx) {
		contexts.remove(cntx);
		showingContext = getMostImportantContext();
	}
	
	private Context getMostImportantContext() {
		Context mostImportant = null;
		for (Iterator iter = contexts.iterator(); iter.hasNext();) {
			Context cntx = (Context)iter.next();
			// if both are equal, the later one wins
			if (mostImportant == null || mostImportant.getPriority() <= cntx.getPriority())
				mostImportant = cntx;
		}
		return mostImportant;
	}
}
